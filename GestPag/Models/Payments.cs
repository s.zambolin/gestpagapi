﻿using System;
using System.Collections.Generic;

namespace GestPag.Models
{
    public partial class Payments
    {
        public int Id { get; set; }
        public int IdUser { get; set; }
        public int Anno { get; set; }
        public decimal Importo { get; set; }

        public Users IdUserNavigation { get; set; }
    }
}
