﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace GestPag.Models
{
    public partial class UtentiContext : DbContext
    {
        public UtentiContext()
        {
        }

        public UtentiContext(DbContextOptions<UtentiContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Payments> Payments { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            /*
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
               optionsBuilder.UseSqlServer(
                   
                   //"Server=STELAB;Database=Utenti;User Id=sa;Password=zamba1234;Trusted_Connection=True;");
            }
            */
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Payments>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Anno).HasColumnName("anno");

                entity.Property(e => e.IdUser).HasColumnName("idUser");

                entity.Property(e => e.Importo)
                    .HasColumnName("importo")
                    .HasColumnType("money");

                entity.HasOne(d => d.IdUserNavigation)
                    .WithMany(p => p.Payments)
                    .HasForeignKey(d => d.IdUser)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Payments_Users");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.Property(e => e.Cognome)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Mobile)
                    .HasColumnName("mobile")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nome)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });
        }
    }
}
