﻿using System;
using System.Collections.Generic;

namespace GestPag.Models
{
    public partial class Users
    {
        public Users()
        {
            Payments = new HashSet<Payments>();
        }
        
        public int Id { get; set; }
        public string Cognome { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }

        public ICollection<Payments> Payments { get; set; }
    }
}
