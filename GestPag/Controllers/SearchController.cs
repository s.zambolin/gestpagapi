﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GestPag.Models;

namespace GestPag.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SearchController : ControllerBase
    {
        private readonly UtentiContext _context;

        public SearchController(UtentiContext context)
        {
            _context = context;
        }

        // GET: api/Search/query
        [HttpGet("{query}")]
        public IEnumerable<Users> GetUsers([FromRoute] string query)
        {
            
            return _context.Users.Where(elem => elem.Cognome.Contains(query) || elem.Nome.Contains(query) || elem.Email.Contains(query) || elem.Mobile.Contains(query));           
        }

    }
}